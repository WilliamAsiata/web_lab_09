/**
 * Created by wasi131 on 29/04/2017.
 */

var timeDiv = document.getElementById("time");
setTime();
var timerId;
document.getElementById("start").onclick = function () {timerId = setInterval(setTime, 1000)};
document.getElementById("stop").onclick = function () {clearInterval(timerId)};

function setTime() {timeDiv.innerHTML = new Date()}
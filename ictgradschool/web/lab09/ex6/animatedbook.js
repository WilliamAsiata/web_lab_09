/**
 * Created by wasi131 on 30/04/2017.
 */

var pages = document.getElementsByClassName("page");

for (var i = 0; i < pages.length; i++) {
    var currentPage = pages[i];
    addClickEvent(currentPage);

    if (i < pages.length - 1){
        var nextPage = pages[i+1];
        currentPage.addEventListener("animationend", setZIndex(nextPage));
        currentPage.addEventListener("webkitAnimationEnd", setZIndex(nextPage));
    }
}

function addClickEvent (page) {
    page.onclick = function (){addAnimationClass()};

    function addAnimationClass() {
        page.classList.add("pageAnimation");
    }
}

function setZIndex(page) {
     return function (){
         page.style.zIndex = 1;
     }
}